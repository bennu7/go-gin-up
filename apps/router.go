package apps

import (
	"fmt"
	"log"

	"gitlab.com/bennu7/go-gin-up/apps/router"
	"gitlab.com/bennu7/go-gin-up/pkg/database"
)

type Router interface {
	BuildRoutes()
	Run()
}

const Router_Gin = "gin"

type RouterFactory struct {
	db *database.Database
}

func NewRouterFactory(db *database.Database) *RouterFactory {
	return &RouterFactory{
		db: db,
	}
}

func (r *RouterFactory) Create(typeRouter, port string) (Router, error) {
	switch typeRouter {
	case Router_Gin:
		return router.NewRouterGin(port, r.db), nil
	default:
		log.Println()
		return nil, fmt.Errorf("router with type %s not found ", typeRouter)
	}
}

type RouterExecutor struct{}

func NewRouterExecutor() *RouterExecutor {
	return &RouterExecutor{}
}

func (*RouterExecutor) Execute(router Router) {
	router.BuildRoutes()
	router.Run()
}
