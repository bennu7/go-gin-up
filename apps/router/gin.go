package router

import (
	"fmt"

	"gitlab.com/bennu7/go-gin-up/apps/domain/follow"

	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/apps/commons/middleware"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth"
	"gitlab.com/bennu7/go-gin-up/pkg/database"
)

type Gin struct {
	port   string
	router *gin.Engine
	db     *database.Database
	middle *middleware.MiddlewareGin
}

func NewRouterGin(port string, db *database.Database) *Gin {
	router := gin.Default()
	router.MaxMultipartMemory = 8 << 20

	middle := middleware.NewMiddlewareGin()
	return &Gin{
		port:   port,
		router: router,
		db:     db,
		middle: middle,
	}
}

func (r *Gin) BuildRoutes() {
	r.router.Use(CORS)

	v1 := r.router.Group("/api/v1")

	authRoute := auth.NewRouterAuth(v1, r.db, r.middle)
	authRoute.RegisterAuthRoutes()

	// v1.Use(r.middle.ValidateAuth)
	friendRoute := follow.NewRouterFollow(v1, r.db, r.middle)
	friendRoute.RegisterFollowRoutes()
}

func CORS(ctx *gin.Context) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	ctx.Header("Access-Control-Allow-Credentials", "true")
	ctx.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	ctx.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	if ctx.Request.Method == "OPTIONS" {
		ctx.AbortWithStatus(204)
		return
	}

	ctx.Next()
}

func (r *Gin) Run() {
	fmt.Println("r.port => ", r.port)
	r.router.Run(r.port)
}
