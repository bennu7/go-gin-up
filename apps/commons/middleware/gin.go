package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/pkg/token"
)

type MiddlewareGin struct {
}

func NewMiddlewareGin() *MiddlewareGin {
	return &MiddlewareGin{}
}

type responseValidateErr struct {
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

func (m *MiddlewareGin) ValidateAuth(ctx *gin.Context) {
	header := ctx.GetHeader("Authorization")
	bearerToken := strings.Split(header, "Bearer ")
	if len(bearerToken) != 2 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, responseValidateErr{
			StatusCode: http.StatusUnauthorized,
			Message:    "token invalid || len token must be 2",
		})
		return
	}

	payload, err := token.ValidateToken(bearerToken[1])
	if err != nil {
		addInfoErr := fmt.Sprintf("token invalid || %s", err.Error())

		ctx.AbortWithStatusJSON(http.StatusUnauthorized, responseValidateErr{
			StatusCode: http.StatusUnauthorized,
			Message:    addInfoErr,
		})
		return
	}

	ctx.Set("AuthId", payload.AuthId)
	ctx.Next()
}
