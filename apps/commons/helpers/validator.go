package helpers

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

func CheckValidatorRequestStruct(model interface{}) ([]interface{}, error) {
	validate := validator.New()
	err := validate.Struct(model)
	var dataError []interface{}
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println("*validator.InvalidValidationError => ", err.(*validator.InvalidValidationError))
			dataError = append(dataError, err.(*validator.InvalidValidationError))
		}

		for _, err := range err.(validator.ValidationErrors) {
			dataErr := fmt.Sprintf("error %s must be %s on %s,", err.Field(), err.Type(), err.ActualTag())
			dataError = append(dataError, dataErr)
		}
		return dataError, err
	}

	return nil, nil
}
