package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/params"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/services"
)

type ControllerAPI struct {
	svc services.FollowSvc
}

func NewControllerAPI(svc services.FollowSvc) *ControllerAPI {
	return &ControllerAPI{
		svc: svc,
	}
}

func (c *ControllerAPI) Follow(ctx *gin.Context) {
	var req = params.FollowingRequest{}

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		resp := response.GeneralErrorWithAdditionalInfo(err.Error())
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	req.AuthId = authId

	custErr := c.svc.FollowFriend(ctx.Request.Context(), authId, &req)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.CreatedSuccess()
	ctx.JSON(resp.StatusCode, resp)
}

func (c *ControllerAPI) GetMyFollowing(ctx *gin.Context) {
	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	myFollowing, custErr := c.svc.GetMyFollowing(ctx.Request.Context(), authId)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.GeneralSuccessCustomMessageAndPayload("GET ALL FOLLLOWING SUCCESS", myFollowing)
	ctx.JSON(resp.StatusCode, resp)
}

func (c *ControllerAPI) UnfollFriend(ctx *gin.Context) {
	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	req := &params.UnfollRequest{}

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		resp := response.GeneralErrorWithAdditionalInfo(err.Error())
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	req.AuthId = authId

	custErr := c.svc.UnfollowFriend(ctx.Request.Context(), authId, req)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.GeneralSuccessCustomMessageAndPayload("UNFOLLOW FRIEND SUCCESS ", nil)
	ctx.JSON(resp.StatusCode, resp)

}
