package params

import (
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/models"
)

type FollowingRequest struct {
	FollowingId int `json:"following_id" validate:"required,number"`
	AuthId      int
}

func (f *FollowingRequest) ParseToModel() *models.Follow {
	// followingId, err := f.FollowingId.Int64()
	// if err != nil {
	// 	return nil
	// }

	return &models.Follow{
		AuthId:      f.AuthId,
		FollowingId: f.FollowingId,
	}
}

type UnfollRequest struct {
	FollowingId int `json:"following_id" validate:"required,number"`
	AuthId      int
}

func (f *UnfollRequest) ParseToModel() *models.Follow {
	return &models.Follow{
		AuthId:      f.AuthId,
		FollowingId: f.FollowingId,
	}
}
