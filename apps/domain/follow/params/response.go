package params

import "gitlab.com/bennu7/go-gin-up/apps/domain/follow/models"

type GetMyFollowingResponse struct {
	Id     int    `json:"id"`
	Email  string `json:"email"`
	ImgUrl string `json:"img_url"`
}

func (g *GetMyFollowingResponse) AddModelToResponse(model *models.FollowWithAuth) {
	g.Id = model.Id
	g.Email = model.Email
	g.ImgUrl = model.Imgurl
}
