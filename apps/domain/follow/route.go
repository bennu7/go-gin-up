package follow

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/apps/commons/middleware"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/controller"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/repositories/postgres"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/services/usecase"
	"gitlab.com/bennu7/go-gin-up/pkg/database"
)

type RouteFollow interface {
	RegisterFollowRoutes()
}

type routeFollowImpl struct {
	router *gin.RouterGroup
	follow *controller.ControllerAPI
	middle *middleware.MiddlewareGin
}

// *register route follow
func (r *routeFollowImpl) RegisterFollowRoutes() {
	follow := r.router.Group("follow")
	{
		follow.Use(r.middle.ValidateAuth)
		follow.POST("", r.follow.Follow)
		follow.GET("", r.follow.GetMyFollowing)
	}

	unfoll := r.router.Group("unfollow")
	{
		unfoll.Use(r.middle.ValidateAuth)
		unfoll.DELETE("", r.follow.UnfollFriend)
	}
}

func NewRouterFollow(r *gin.RouterGroup, db *database.Database, middle *middleware.MiddlewareGin) RouteFollow {
	repo := postgres.NewFollowRepo(db.DbSQL)
	svc := usecase.NewFollowSvc(repo)
	handler := controller.NewControllerAPI(svc)

	return &routeFollowImpl{
		router: r,
		follow: handler,
		middle: middle,
	}
}
