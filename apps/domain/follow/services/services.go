package services

import (
	"context"

	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/params"
)

type FollowSvc interface {
	FollowFriend(ctx context.Context, currentAuthId int, req *params.FollowingRequest) *response.CustomError
	GetMyFollowing(ctx context.Context, authId int) ([]*params.GetMyFollowingResponse, *response.CustomError)
	UnfollowFriend(ctx context.Context, currentAuthId int, req *params.UnfollRequest) *response.CustomError
}
