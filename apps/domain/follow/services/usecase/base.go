package usecase

import (
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/repositories"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/services"
)

type followSvc struct {
	repo repositories.FollowRepo
}

func NewFollowSvc(repo repositories.FollowRepo) services.FollowSvc {
	return &followSvc{
		repo: repo,
	}
}
