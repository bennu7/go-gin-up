package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/bennu7/go-gin-up/apps/commons/helpers"
	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/params"
)

func (f *followSvc) FollowFriend(ctx context.Context, currentAuthId int, req *params.FollowingRequest) *response.CustomError {
	errorsStruct, err := helpers.CheckValidatorRequestStruct(req)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(errorsStruct)
	}

	follow := req.ParseToModel()
	follow.CreatedAt = time.Now()
	follow.AuthId = currentAuthId

	isBool, err := f.repo.CheckFollowingExists(ctx, follow)
	if !isBool {
		return response.NotFoundErrorWithAdditionalInfo("FOLLOWING ID NOT FOUND")
	}
	if err != nil {
		return response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	err = f.repo.Create(ctx, follow)
	fmt.Println("lanjut proses Create")
	if err != nil {
		return response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	return nil
}

func (f *followSvc) UnfollowFriend(ctx context.Context, currentAuthId int, req *params.UnfollRequest) *response.CustomError {
	errorsStruct, err := helpers.CheckValidatorRequestStruct(req)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(errorsStruct)
	}

	unfoll := req.ParseToModel()

	isWasDeleted, err := f.repo.Delete(ctx, currentAuthId, unfoll.FollowingId)
	if isWasDeleted == 0 {
		return response.NotFoundErrorWithAdditionalInfo("following_id not found OR status unfollowed")
	}
	if err != nil {
		return response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	return nil
}
