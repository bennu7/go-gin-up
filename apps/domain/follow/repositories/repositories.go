package repositories

import (
	"context"

	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/models"
)

type FollowRepo interface {
	Create(ctx context.Context, follow *models.Follow) error
	CheckFollowingExists(ctx context.Context, follow *models.Follow) (bool, error)
	GetAll(ctx context.Context, authId int) ([]*models.FollowWithAuth, error)
	Delete(ctx context.Context, authId, followingId int) (int64, error)
}
