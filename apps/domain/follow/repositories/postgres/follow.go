package postgres

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/repositories"
	"gorm.io/gorm"

	"gitlab.com/bennu7/go-gin-up/apps/domain/follow/models"
)

var (
	queryCreate = `INSERT INTO follows (authid, followingid, created_at) VALUES ($1, $2, $3)`

	queryGetAllByAuthId = `
		SELECT a.id, a.email, a.img_url
		FROM follows as f
		JOIN auths as a
			ON a.id = f.followingid
		WHERE 
		    f.authid = $1
	`

	queryUnfollow = `
		DELETE from follows as f
		WHERE 
		    f.authid = $1
		AND 
		    f.followingid = $2
	`

	queryCheckExists = `
	SELECT EXISTS(
        SELECT * from auths WHERE id = $1
    ) as is_exist;
	`
)

type followRepo struct {
	db *gorm.DB
}

func NewFollowRepo(db *gorm.DB) repositories.FollowRepo {
	return &followRepo{
		db: db,
	}
}

// *create repo.FollowRepo
func (f *followRepo) Create(ctx context.Context, follow *models.Follow) error {
	result := f.db.Debug().Raw(queryCreate, follow.AuthId, follow.FollowingId, follow.CreatedAt).Row()
	if result.Err() != nil {
		return result.Err()
	}

	return result.Err()
}

// *check data before create repo.FollowRepo
func (f *followRepo) CheckFollowingExists(ctx context.Context, follow *models.Follow) (bool, error) {
	result := f.db.Debug().Raw(queryCheckExists, follow.FollowingId)
	if result.Error != nil {
		return false, result.Error
	}

	var is_exist bool
	result.Scan(&is_exist)
	if !is_exist {
		return false, errors.New("status following_id has followed")
	}

	return true, nil
}

// *delete repo.FollowRepo
func (f *followRepo) Delete(ctx context.Context, authId, followingId int) (int64, error) {
	follow := new(models.Follow)
	fmt.Println(authId, followingId)
	// result := f.db.Raw(queryUnfollow, authId, followingId)
	result := f.db.Debug().Delete(follow, "authid = $1 and followingid = $2", authId, followingId)
	if result.Error != nil {
		return 0, result.Error
	}

	valueInt := result.RowsAffected
	fmt.Println("valueInt : ", valueInt)

	return valueInt, nil
}

// *getAll repo.FollowRepo
func (f *followRepo) GetAll(ctx context.Context, authId int) ([]*models.FollowWithAuth, error) {
	result := f.db.Debug().Model(&models.Follow{}).Raw(queryGetAllByAuthId, authId)
	if result.Error != nil {
		return nil, result.Error
	}

	rows, err := result.Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var follows []*models.FollowWithAuth
	for rows.Next() {
		currFollowAuth := models.FollowWithAuth{}

		err := rows.Scan(
			&currFollowAuth.Id,
			&currFollowAuth.Email,
			&currFollowAuth.Imgurl,
		)

		if err != nil {
			return nil, err
		}

		follows = append(follows, &currFollowAuth)
	}
	return follows, nil
}
