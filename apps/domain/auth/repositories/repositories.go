package repositories

import (
	"context"

	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/models"
)

type AuthRepo interface {
	Create(ctx context.Context, auth *models.Auth) error
	FindByEmail(ctx context.Context, email string) (*models.Auth, error)
	SearchAuthByEmail(ctx context.Context, email string, id int) ([]*models.SearchAuth, error)
	FindById(ctx context.Context, authId int) (*models.Auth, error)
	Delete(ctx context.Context, authId int) error
}
