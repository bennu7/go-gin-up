package postgres

import (
	"context"
	"fmt"

	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/repositories"
	"gorm.io/gorm"

	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/models"
)

var (
	// queryCreate = `
	// 	INSERT INTO auths (email, password, img_url)
	// 	VALUES ($1, $2, $3)
	// `

	// queryFindEmail = `
	// 	SELECT id, email, password
	// 	FROM auths
	// 	WHERE email=$1
	// `

	querySearchByEmail = `
		SELECT a.email, a.id, COALESCE(a.img_url, '') AS img_url, f.followingId, f.authId 
		FROM auths as a 
		LEFT JOIN follows as f
			ON a.id = f.followingId
		WHERE (a.id <> $1
		OR f.authId=$1)
		AND a.email LIKE $2
		ORDER BY a.id DESC
	`

	// querySearchByEmailGORM = `
	// 	SELECT a.email, a.id, COALESCE(a.img_url, '') AS img_url, f.followingId, f.authId
	// 	FROM auth as a
	// 	LEFT JOIN follows as f
	// 		ON a.id = f.followingId
	// 	WHERE (a.id <> $1
	// 	OR f.authId=$1)
	// 	AND a.email LIKE $2
	// 	ORDER BY a.id DESC
	// `

	queryFindById = `
		SELECT id, email, 
		       COALESCE(img_url, '') AS img_url, 
		       COALESCE(img_key, '') AS img_key
		FROM auths
		WHERE id=$1
	`
)

type authRepo struct {
	db *gorm.DB
}

func NewAuthRepo(db *gorm.DB) repositories.AuthRepo {
	return &authRepo{
		db: db,
	}
}

func (a *authRepo) FindById(ctx context.Context, authId int) (*models.Auth, error) {
	var auth = models.Auth{}
	result := a.db.Debug().Raw(queryFindById, authId).First(&auth)
	if result.Error != nil {
		return nil, result.Error
	}

	fmt.Println(auth)
	return &auth, nil
}

func (a *authRepo) FindByEmail(ctx context.Context, email string) (*models.Auth, error) {

	var auth = new(models.Auth)
	result := a.db.Debug().Where("email = ?", email).First(&auth)
	if result.Error != nil {
		return nil, result.Error
	}

	data := result.Scan(auth)
	if data.Error != nil {
		return nil, data.Error
	}

	return auth, nil
}

func (a *authRepo) SearchAuthByEmail(ctx context.Context, email string, id int) ([]*models.SearchAuth, error) {
	result := a.db.Debug().Raw(querySearchByEmail, id, "%"+email+"%")
	if result.Error != nil {
		return nil, result.Error
	}

	rows, err := result.Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var auth []*models.SearchAuth
	for rows.Next() {
		currentAuth := &models.SearchAuth{}
		err := rows.Scan(
			&currentAuth.Email,
			&currentAuth.Id,
			&currentAuth.ImgUrl,
			&currentAuth.FollowingId,
			&currentAuth.AuthId,
		)

		if err != nil {
			return nil, err
		}

		auth = append(auth, currentAuth)
	}

	return auth, nil
}

func (a *authRepo) Create(ctx context.Context, auth *models.Auth) error {
	data := a.db.Debug().Model(&models.Auth{}).Create(auth)

	if data.Error != nil {
		return data.Error
	}

	return nil
}

func (a *authRepo) Delete(ctx context.Context, authId int) error {
	auths := new(models.Auth)
	data := a.db.Debug().Model(auths).Delete(auths, authId)
	if data.Error != nil {
		return data.Error
	}

	fmt.Println("RowsAffected => ", data.RowsAffected)

	return nil
}
