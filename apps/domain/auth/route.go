package auth

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/apps/commons/middleware"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/controller"
	postgres "gitlab.com/bennu7/go-gin-up/apps/domain/auth/repositories/postgres"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/services/usecase"
	"gitlab.com/bennu7/go-gin-up/pkg/database"
)

type RouteAuth interface {
	RegisterAuthRoutes()
}

type routeAuthImpl struct {
	route  *gin.RouterGroup
	auth   *controller.ControllerAPI
	middle *middleware.MiddlewareGin
}

func NewRouterAuth(r *gin.RouterGroup, db *database.Database, middle *middleware.MiddlewareGin) RouteAuth {
	repo := postgres.NewAuthRepo(db.DbSQL)
	svc := usecase.NewAuthSvc(repo)
	handler := controller.NewControllerAPI(svc)
	return &routeAuthImpl{
		route:  r,
		auth:   handler,
		middle: middle,
	}
}

func (r *routeAuthImpl) RegisterAuthRoutes() {
	auth := r.route.Group("auth")
	{
		auth.POST("/register", r.auth.Register)
		auth.POST("/login", r.auth.Login)
		// auth.POST("/file", r.auth.Upload)

		auth.Use(r.middle.ValidateAuth)
		auth.DELETE("/delete", r.auth.Delete)
		auth.GET("/profile", r.auth.Profile)
		auth.POST("/search", r.auth.Search)
	}
}
