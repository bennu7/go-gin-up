package services

import (
	"context"
	"mime/multipart"

	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/params"
)

type AuthSvc interface {
	Register(ctx context.Context, req *params.UserRegisterRequest) *response.CustomError
	Search(ctx context.Context, email string, id int, req *params.UserSearchRequest) ([]*params.UserSearchResponse, *response.CustomError)
	Login(ctx context.Context, req *params.UserLoginRequest) (*params.UserLoginResponse, *response.CustomError)
	Profile(ctx context.Context, authId int) (*params.UserProfileResponse, *response.CustomError)
	UploadFileS3(ctx context.Context, fileName string, file multipart.File) (string, string, *response.CustomError)
	DeleteFileS3(ctx context.Context) *response.CustomError
	Delete(ctx context.Context, authId int) *response.CustomError
}
