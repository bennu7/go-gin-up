package usecase

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"

	"gitlab.com/bennu7/go-gin-up/apps/commons/helpers"
	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/params"
	"gitlab.com/bennu7/go-gin-up/pkg/encryption"
)

// *register implement services.AuthSvc
func (a *authSvc) Register(ctx context.Context, req *params.UserRegisterRequest) *response.CustomError {
	errorsStruct, err := helpers.CheckValidatorRequestStruct(req)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(errorsStruct)
	}

	reqModel := req.ParseToModel()
	hashPass, err := encryption.HashPassword(reqModel.Password)
	if err != nil {
		return response.GeneralError()
	}

	reqModel.Password = hashPass

	err = a.repo.Create(ctx, reqModel)
	if err != nil {
		return response.RepositoryErrorWithAdditionalInfo(err.Error())
	}
	return nil
}

func (a *authSvc) Delete(ctx context.Context, authId int) *response.CustomError {
	// *first check data
	findData, err := a.repo.FindById(ctx, authId)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(err.Error())
	}

	// *second delete data image if exists
	s3Client := a.S3Client.ConfigAws()
	_, err = s3Client.DeleteObject(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String("golang-upload"),
		Key:    aws.String(findData.ImgKey),
	})

	err = a.repo.Delete(ctx, authId)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(err.Error())
	}

	return nil
}
