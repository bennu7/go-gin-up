package usecase

import (
	"context"
	"database/sql"
	"fmt"
	"mime/multipart"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/bennu7/go-gin-up/pkg/encryption"
	"gitlab.com/bennu7/go-gin-up/pkg/token"

	"gitlab.com/bennu7/go-gin-up/apps/commons/helpers"
	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/params"
)

func (a *authSvc) Search(ctx context.Context, email string, id int, req *params.UserSearchRequest) ([]*params.UserSearchResponse, *response.CustomError) {
	errorsStruct, err := helpers.CheckValidatorRequestStruct(req)
	if err != nil {
		return nil, response.BadRequestErrorWithAdditionalInfo(errorsStruct)
	}

	auth, err := a.repo.SearchAuthByEmail(ctx, email, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, response.NotFoundError()
		}

		return nil, response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	var authResp []*params.UserSearchResponse
	for _, search := range auth {
		currentAuthResp := &params.UserSearchResponse{}
		currentAuthResp.ParseToModelResponse(search)

		currentAuthResp.IsFollow = false
		if search.AuthId != nil {
			if *search.AuthId == id {
				currentAuthResp.IsFollow = true
			}
		}
		authResp = append(authResp, currentAuthResp)
	}

	if authResp == nil {
		authResp = []*params.UserSearchResponse{}
	}

	return authResp, nil
}

func (a *authSvc) Login(ctx context.Context, req *params.UserLoginRequest) (*params.UserLoginResponse, *response.CustomError) {
	errorsStruct, err := helpers.CheckValidatorRequestStruct(req)
	if err != nil {
		return nil, response.BadRequestErrorWithAdditionalInfo(errorsStruct)
	}

	auth, err := a.repo.FindByEmail(ctx, req.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, response.NotFoundError()
		}

		return nil, response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	err = encryption.ValidatePassword(auth.Password, req.Password)
	if err != nil {
		return nil, response.NotFoundError()
	}

	token, err := token.GenerateToken(auth.Id)
	if err != nil {
		return nil, response.GeneralErrorWithAdditionalInfo(err.Error())
	}

	return &params.UserLoginResponse{
		Token: token,
	}, nil

}

func (a *authSvc) Profile(ctx context.Context, authId int) (*params.UserProfileResponse, *response.CustomError) {
	auth, err := a.repo.FindById(ctx, authId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, response.NotFoundError()
		}

		return nil, response.RepositoryErrorWithAdditionalInfo(err.Error())
	}

	authResp := &params.UserProfileResponse{}
	authResp.ParseToModelResponse(auth)

	return authResp, nil
}

func (a *authSvc) UploadFileS3(ctx context.Context, fileName string, file multipart.File) (string, string, *response.CustomError) {
	configAws := a.S3Client.ConfigAws()
	uploader := manager.NewUploader(configAws)

	// *proses uploader
	uploadOutput, err := uploader.Upload(ctx, &s3.PutObjectInput{
		Bucket: aws.String("golang-upload"),
		Key:    aws.String(fileName),
		Body:   file,
		ACL:    "public-read",
	})
	if err != nil {
		return "", "", response.BadRequestErrorWithAdditionalInfo(err.Error())
	}

	fmt.Println("upload Output ", uploadOutput.Location, uploadOutput.Key)
	// * untuk uploadOutput.Location dan *uploadOutput.Key merupakan urusan repository
	return uploadOutput.Location, *uploadOutput.Key, nil
}

func (a *authSvc) DeleteFileS3(ctx context.Context) *response.CustomError {
	s3Client := a.S3Client.ConfigAws()

	objOutput, err := s3Client.DeleteObject(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String("golang-upload"),
		Key:    aws.String("github.png"),
	})
	fmt.Println("objOutput => ", objOutput)
	fmt.Println("status deleteMarker ", objOutput.DeleteMarker)
	if err != nil {
		return response.BadRequestErrorWithAdditionalInfo(err.Error())
	}

	return nil
}
