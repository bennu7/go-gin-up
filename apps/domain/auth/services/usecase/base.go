package usecase

import (
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/repositories"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/services"
	"gitlab.com/bennu7/go-gin-up/pkg/AWS"
)

type authSvc struct {
	repo repositories.AuthRepo
	//S3Client *s3.Client
	S3Client *AWS.ConfigAwsStruct
}

func NewAuthSvc(repo repositories.AuthRepo) services.AuthSvc {
	return &authSvc{
		repo: repo,
	}
}
