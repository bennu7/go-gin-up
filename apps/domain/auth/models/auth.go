package models

type Auth struct {
	Id       int
	Email    string
	Password string
	ImgUrl   string
	ImgKey   string
}

type SearchAuth struct {
	Id          int
	Email       string
	ImgUrl      string
	FollowingId *int
	AuthId      *int
}
