package params

import "gitlab.com/bennu7/go-gin-up/apps/domain/auth/models"

type UserLoginResponse struct {
	Token string `json:"token"`
}

type UserSearchResponse struct {
	Id       int    `json:"id"`
	Email    string `json:"email"`
	ImgUrl   string `json:"img_url"`
	IsFollow bool   `json:"is_follow"`
}

func (u *UserSearchResponse) ParseToModelResponse(model *models.SearchAuth) {
	u.Id = model.Id
	u.Email = model.Email
	u.ImgUrl = model.ImgUrl
}

type UserProfileResponse struct {
	Id     int    `json:"id"`
	Email  string `json:"email"`
	ImgUrl string `json:"img_url"`
}

func (u *UserProfileResponse) ParseToModelResponse(model *models.Auth) {
	u.Id = model.Id
	u.Email = model.Email
	u.ImgUrl = model.ImgUrl
}
