package params

import (
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/models"
)

type UserLoginRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type UserSearchRequest struct {
	Email  string `json:"email" validate:"required,email"`
	AuthId int
}

type UserRegisterRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	ImgUrl   string
	ImgKey   string
}

func (u *UserRegisterRequest) ParseToModel() *models.Auth {
	return &models.Auth{
		Email:    u.Email,
		Password: u.Password,
		ImgUrl:   u.ImgUrl,
		ImgKey:   u.ImgKey,
	}
}
