package controller

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/bennu7/go-gin-up/apps/commons/response"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/params"
	"gitlab.com/bennu7/go-gin-up/apps/domain/auth/services"
)

type ControllerAPI struct {
	svc services.AuthSvc
}

func NewControllerAPI(svc services.AuthSvc) *ControllerAPI {
	return &ControllerAPI{svc: svc}
}

func (c *ControllerAPI) Register(ctx *gin.Context) {
	// *handle file upload to AWS S3
	file, err := ctx.FormFile("image")
	if err != nil {
		resp := response.GeneralErrorWithAdditionalInfo(err.Error())
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}
	f, err := file.Open()
	if err != nil {
		resp := response.GeneralErrorWithAdditionalInfo(err.Error())
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}
	defer f.Close()

	imgUrl, imgKey, custErr := c.svc.UploadFileS3(ctx, file.Filename, f)
	if custErr != nil {
		ctx.JSON(custErr.StatusCode, custErr)
		return
	}

	// *handle email & password
	email := ctx.PostForm("email")
	password := ctx.PostForm("password")
	fmt.Println(email, password)
	var req = &params.UserRegisterRequest{
		Email:    email,
		Password: password,
		ImgUrl:   imgUrl,
		ImgKey:   imgKey,
	}

	custErr = c.svc.Register(ctx.Request.Context(), req)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.CreatedSuccess()
	ctx.JSON(resp.StatusCode, resp)
}

func (c *ControllerAPI) Delete(ctx *gin.Context) {
	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	custErr := c.svc.Delete(ctx.Request.Context(), authId)
	if custErr != nil {
		ctx.JSON(custErr.StatusCode, custErr)
		return
	}

	ctx.JSON(200, response.GeneralSuccessCustomMessageAndPayload("SUCCESS DELETE", ""))
}

func (c *ControllerAPI) Search(ctx *gin.Context) {
	var req = &params.UserSearchRequest{}

	err := ctx.ShouldBindJSON(req)
	if err != nil {
		resp := response.GeneralError()
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	// *get id from authorize
	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	req.AuthId = authId
	result, custErr := c.svc.Search(ctx.Request.Context(), req.Email, authId, req)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.GeneralSuccessCustomMessageAndPayload("SEARCH FOLLOWED SUCCESS", result)
	ctx.JSON(resp.StatusCode, resp)
}

func (c *ControllerAPI) Login(ctx *gin.Context) {
	var req = &params.UserLoginRequest{}

	err := ctx.ShouldBindJSON(req)
	if err != nil {
		resp := response.GeneralError()
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}
	result, custErr := c.svc.Login(ctx.Request.Context(), req)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.GeneralSuccessCustomMessageAndPayload("LOGIN SUCCESS", result)
	ctx.JSON(resp.StatusCode, resp)
}

func (c *ControllerAPI) Profile(ctx *gin.Context) {
	authId := ctx.GetInt("AuthId")
	if authId == 0 {
		resp := response.GeneralErrorWithAdditionalInfo("auth id can't be 0")
		ctx.AbortWithStatusJSON(resp.StatusCode, resp)
		return
	}

	profile, custErr := c.svc.Profile(ctx.Request.Context(), authId)
	if custErr != nil {
		ctx.AbortWithStatusJSON(custErr.StatusCode, custErr)
		return
	}

	resp := response.GeneralSuccessCustomMessageAndPayload("GET PROFILE SUCCESS", profile)
	ctx.JSON(resp.StatusCode, resp)
}
