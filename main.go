package main

import (
	"log"
	"os"

	"gitlab.com/bennu7/go-gin-up/apps"
	"gitlab.com/bennu7/go-gin-up/config"
	"gitlab.com/bennu7/go-gin-up/pkg/database"
)

func main() {
	config.LoadConfig(".env")
	// sqlDb, err := database.ConnectDB()
	sqlDb, err := database.ConnectDBGORM()
	if err != nil {
		log.Fatal(err)
	}

	//// *AWS S3
	//sdkConfig, err := configAWS.LoadDefaultConfig(context.TODO())
	//if err != nil {
	//	fmt.Println("Couldn't load default configuration. Have you set up your AWS account?")
	//	fmt.Println(err)
	//	return
	//}
	//
	//S3Client := s3.NewFromConfig(sdkConfig)
	//
	//object, err := S3Client.DeleteObject(context.TODO(), &s3.DeleteObjectInput{
	//	Bucket: aws.String("golang-upload"),
	//	Key:    aws.String("github.png"),
	//})
	//if err != nil {
	//	fmt.Println("error delete, ", err.Error())
	//	return
	//}
	//fmt.Println("done delete ", object.DeleteMarker)
	//fmt.Println("S3Client => ", S3Client)

	db := database.NewDatabase()
	db = db.SetSQl(sqlDb)

	port := os.Getenv("PORT")
	factory := apps.NewRouterFactory(db)
	router, err := factory.Create(apps.Router_Gin, ":"+port)
	if err != nil {
		panic(err)
	}

	executor := apps.NewRouterExecutor()
	executor.Execute(router)
}
