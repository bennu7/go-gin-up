package database

import (
	"gorm.io/gorm"
)

type Database struct {
	DbSQL *gorm.DB
}

func NewDatabase() *Database {
	return &Database{}
}

func (d *Database) SetSQl(db *gorm.DB) *Database {
	d.DbSQL = db
	return d
}
