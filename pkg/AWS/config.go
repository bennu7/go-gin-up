package AWS

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type ConfigAwsStruct struct {
	Client *s3.Client
}

func NewConfigAwsStruct(client *s3.Client) *ConfigAwsStruct {
	return &ConfigAwsStruct{Client: client}
}

//func (c *ConfigAwsStruct) ConfigAws() *ConfigAwsStruct {
//	// *AWS S3
//	sdkConfig, err := config.LoadDefaultConfig(context.TODO())
//	if err != nil {
//		fmt.Println("error AWS SDK GO V2, ", err)
//		return nil
//	}
//
//	S3Client := s3.NewFromConfig(sdkConfig)
//	c.Client = S3Client
//	return c
//}

func (c *ConfigAwsStruct) ConfigAws() *s3.Client {
	// *AWS S3
	sdkConfig, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		fmt.Println("error AWS SDK GO V2, ", err)
		return nil
	}

	S3Client := s3.NewFromConfig(sdkConfig)
	return S3Client
}
