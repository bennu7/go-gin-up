package token

type Token struct {
	AuthId  int
	Expired int64
}
