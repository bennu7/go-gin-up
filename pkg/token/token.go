package token

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
)

const (
	// *this key must be len 32 character
	TOKEN_KEY     = "ImWDKthBUGCVWCtXHyWbpcwM7SeBHIEd"
	TOKEN_EXPIRED = 60 * 60 * time.Second
)

func ValidateToken(tokString string) (*Token, error) {
	tok, err := jwt.Parse(tokString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(TOKEN_KEY), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok && !tok.Valid {
		return nil, errors.New("token invalid")
	}

	getPayload := claims["payload"]
	var payloadToken = Token{}

	// *convert mapping to Token{}
	payloadByte, err := json.Marshal(getPayload)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(payloadByte, &payloadToken.AuthId)
	if err != nil {
		return nil, err
	}

	return &payloadToken, nil
}

func GenerateToken(authId int) (string, error) {
	payload := Token{
		AuthId:  authId,
		Expired: time.Now().Add(TOKEN_EXPIRED).Unix(),
	}

	claims := jwt.MapClaims{
		"payload": payload.AuthId,
		// "exp":     payload.Expired,
	}
	claims["exp"] = payload.Expired

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString([]byte(TOKEN_KEY))
	if err != nil {
		return "", err
	}

	return tokenStr, nil
}
