-- Active: 1678072278724@@containers-us-west-168.railway.app@6347@railway
CREATE TABLE auths (
    id SERIAL PRIMARY KEY,
    email VARCHAR,
    img_url VARCHAR,
    img_key VARCHAR,
    password VARCHAR,
    UNIQUE(email)
);

CREATE TABLE follows (
    id SERIAL PRIMARY KEY,
    authId INT NOT NULL,
    followingId INT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    UNIQUE(authId, followingId)
);

drop Table IF EXISTS auths;