package config

import (
	"fmt"

	"github.com/joho/godotenv"
)

// *first config, for access .env
func LoadConfig(filename string) {
	err := godotenv.Load(filename)
	if err != nil {
		fmt.Println("err load config ", err)
	}
}
